\name{collr2-package}
\alias{collr2-package}
\alias{collr2}
\docType{package}
\title{
An R package for quantitative CDH analysis
}
\description{
Compound Double Heterozygosity test (CDH) is a statistical test for detecting compound heterozygosity (as the name suggests).
Quantitative CDH (qCDH) extends the method to support general linear models and functionally a superset of CDH. This package implements 
qCDH in a user friendly and performant way.
}
\details{
\tabular{ll}{
Package: \tab collr2\cr
Type: \tab Package\cr
Version: \tab 1.2\cr
Date: \tab 2014-10-26\cr
License: \tab GPL \cr
}
}
\author{
Kaiyin Zhong, Fan Liu
Maintainer: Kaiyin Zhong <kindlychung@gmail.com>
}
\references{
To be updated.
}
\keyword{ package, GWAS, qCDH, CDH, compound heterozygosity }
\seealso{
}
\examples{
}
